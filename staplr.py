# all the imports
from flask import Flask, request, session, g, redirect, url_for, \
     abort, render_template, flash
from flaskext.markdown import Markdown

# configuration
DEBUG = True
SECRET_KEY = 'development key'
USERNAME = 'admin'
PASSWORD = 'default'

# create our little application :)
app = Flask(__name__)
app.config.from_object(__name__)

app.config.from_envvar('FLASKR_SETTINGS', silent=True)

Markdown(app)

@app.route('/')
def main():
    return render_template('main.html')
    
@app.route('/body')
def mkd():
    return render_template('body.html')

if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0')

